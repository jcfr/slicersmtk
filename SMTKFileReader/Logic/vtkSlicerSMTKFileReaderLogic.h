//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.
 
  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/

#ifndef __vtkSlicerSMTKFileReaderLogic_h
#define __vtkSlicerSMTKFileReaderLogic_h

// Slicer includes
#include "vtkSlicerModuleLogic.h"

// STD includes
#include <vector>

// SMTKFileReader includes
#include "vtkSlicerSMTKFileReaderLogicExport.h"

class vtkMRMLScalarVolumeNode;
class vtkMRMLScalarVolumeDisplayNode;
class vtkMRMLVolumeHeaderlessStorageNode;
class vtkStringArray;

/// \ingroup SlicerRt_QtModules_SMTKFileReader
class VTK_SLICER_SMTKFILEREADER_LOGIC_EXPORT vtkSlicerSMTKFileReaderLogic :
  public vtkSlicerModuleLogic
{
public:
  static vtkSlicerSMTKFileReaderLogic *New();
  vtkTypeMacro(vtkSlicerSMTKFileReaderLogic, vtkSlicerModuleLogic);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  vtkMRMLScalarVolumeNode* LoadSMTKFile(char* filename);
  
protected:
  vtkSlicerSMTKFileReaderLogic();
  ~vtkSlicerSMTKFileReaderLogic() override;

private:
  vtkSlicerSMTKFileReaderLogic(const vtkSlicerSMTKFileReaderLogic&) = delete;
  void operator=(const vtkSlicerSMTKFileReaderLogic&) = delete;
};

#endif