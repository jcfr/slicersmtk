//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.
 
  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/

// SMTKFileReader includes
#include "vtkSlicerSMTKFileReaderLogic.h"

// VTK includes
#include <vtkImageData.h>
#include <vtkMatrix4x4.h>
#include <vtkImageShiftScale.h>
#include <vtkObjectFactory.h>

// MRML includes
#include <vtkMRMLScalarVolumeNode.h>
#include <vtkMRMLScalarVolumeDisplayNode.h>
#include <vtkMRMLScene.h>
#include <vtkMRMLSelectionNode.h>

// Slicer logic includes
#include <vtkSlicerApplicationLogic.h>

// STD includes
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <functional>

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSlicerSMTKFileReaderLogic);

//----------------------------------------------------------------------------
vtkSlicerSMTKFileReaderLogic::vtkSlicerSMTKFileReaderLogic() = default;

//----------------------------------------------------------------------------
vtkSlicerSMTKFileReaderLogic::~vtkSlicerSMTKFileReaderLogic() = default;

//----------------------------------------------------------------------------
void vtkSlicerSMTKFileReaderLogic::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
vtkMRMLScalarVolumeNode* vtkSlicerSMTKFileReaderLogic::LoadSMTKFile(char* filename)
{
  vtkSmartPointer<vtkMRMLScalarVolumeNode> vffVolumeNode = vtkSmartPointer<vtkMRMLScalarVolumeNode>::New();
  return vffVolumeNode.GetPointer();
}
