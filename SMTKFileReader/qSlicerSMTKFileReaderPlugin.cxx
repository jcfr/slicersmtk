/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

// SlicerQt includes
#include "qSlicerSMTKFileReaderPlugin.h"
#include "qSlicerSMTKFileReaderOptionsWidget.h"

// Logic includes
#include "vtkSlicerSMTKFileReaderLogic.h"

// MRML includes
#include <vtkMRMLScalarVolumeNode.h>

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileReader
class qSlicerSMTKFileReaderPluginPrivate
{
  public:
  vtkSmartPointer<vtkSlicerSMTKFileReaderLogic> Logic;
};

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderPlugin::qSlicerSMTKFileReaderPlugin(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileReaderPluginPrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderPlugin::qSlicerSMTKFileReaderPlugin(vtkSlicerSMTKFileReaderLogic* logic, QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileReaderPluginPrivate)
{
  this->setLogic(logic);
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderPlugin::~qSlicerSMTKFileReaderPlugin() = default;

//-----------------------------------------------------------------------------
void qSlicerSMTKFileReaderPlugin::setLogic(vtkSlicerSMTKFileReaderLogic* logic)
{
  Q_D(qSlicerSMTKFileReaderPlugin);
  d->Logic = logic;
}

//-----------------------------------------------------------------------------
vtkSlicerSMTKFileReaderLogic* qSlicerSMTKFileReaderPlugin::logic()const
{
  Q_D(const qSlicerSMTKFileReaderPlugin);
  return d->Logic.GetPointer();
}

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileReaderPlugin::description()const
{
  return "SMTK";
}

//-----------------------------------------------------------------------------
qSlicerIO::IOFileType qSlicerSMTKFileReaderPlugin::fileType()const
{
  return QString("SMTK File");
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileReaderPlugin::extensions()const
{
  return QStringList() << "SMTK (*.smtk)";
}

//-----------------------------------------------------------------------------
qSlicerIOOptions* qSlicerSMTKFileReaderPlugin::options()const
{
  return new qSlicerSMTKFileReaderOptionsWidget;
}

