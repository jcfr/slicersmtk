//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/
// Qt includes
#include <QDebug>

// SlicerQt includes
#include <qSlicerCoreApplication.h>
#include <qSlicerIOManager.h>
#include <qSlicerNodeWriter.h>

// SMTKFileReader Logic includes
#include <vtkSlicerSMTKFileReaderLogic.h>

// SMTKFileReader QTModule includes
#include "qSlicerSMTKFileReaderPlugin.h"
#include "qSlicerSMTKFileReaderModule.h"
#include "qSlicerSMTKFileReaderPluginWidget.h"


//-----------------------------------------------------------------------------
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtPlugin>
Q_EXPORT_PLUGIN2(qSlicerSMTKFileReaderModule, qSlicerSMTKFileReaderModule);
#endif

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileReader
class qSlicerSMTKFileReaderModulePrivate
{
public:
  qSlicerSMTKFileReaderModulePrivate();
};

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderModulePrivate::qSlicerSMTKFileReaderModulePrivate() = default;


//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderModule::qSlicerSMTKFileReaderModule(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileReaderModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderModule::~qSlicerSMTKFileReaderModule() = default;

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileReaderModule::helpText()const
{
  QString help = QString(
    "The SMTKFileReader module enables importing and loading SMTK files into Slicer.<br>");
  return help;
}

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileReaderModule::acknowledgementText()const
{
  QString acknowledgement = QString(
    "This work is part of the AEVA project");
  return acknowledgement;
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileReaderModule::contributors()const
{
  QStringList moduleContributors;
  moduleContributors << QString("Andinet Enquobahrie");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileReaderModule::categories()const
{
  return QStringList() << "smtk IO";
}

//-----------------------------------------------------------------------------
void qSlicerSMTKFileReaderModule::setup()
{
  this->Superclass::setup();
  
  vtkSlicerSMTKFileReaderLogic* SMTKFileReaderLogic =  
    vtkSlicerSMTKFileReaderLogic::SafeDownCast(this->logic());

  qDebug() << "Setting up SMTKFileReader.";
  // Adds the module to the IO Manager
  qSlicerCoreIOManager* ioManager =
    qSlicerCoreApplication::application()->coreIOManager();
  ioManager->registerIO(new qSlicerSMTKFileReaderPlugin(SMTKFileReaderLogic,this));
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation* qSlicerSMTKFileReaderModule::createWidgetRepresentation()
{
  return new qSlicerSMTKFileReaderPluginWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerSMTKFileReaderModule::createLogic()
{
  return vtkSlicerSMTKFileReaderLogic::New();
}

