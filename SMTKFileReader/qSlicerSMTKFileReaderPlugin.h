/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

#ifndef __qSlicerSMTKFileReaderPlugin
#define __qSlicerSMTKFileReaderPlugin

// SlicerQt includes
#include "qSlicerFileReader.h"

class qSlicerSMTKFileReaderPluginPrivate;
class vtkSlicerSMTKFileReaderLogic;

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileReader
class qSlicerSMTKFileReaderPlugin
  : public qSlicerFileReader
{
  Q_OBJECT

public:
  typedef qSlicerFileReader Superclass;
  qSlicerSMTKFileReaderPlugin(QObject* parent = nullptr);
  qSlicerSMTKFileReaderPlugin(vtkSlicerSMTKFileReaderLogic* logic, QObject* parent = nullptr);
  ~qSlicerSMTKFileReaderPlugin() override;

  vtkSlicerSMTKFileReaderLogic* logic()const;
  void setLogic(vtkSlicerSMTKFileReaderLogic* logic);

  QString description()const override;
  IOFileType fileType()const override;
  QStringList extensions()const override;
  qSlicerIOOptions* options()const override;

protected:
  QScopedPointer<qSlicerSMTKFileReaderPluginPrivate> d_ptr;

private:
  Q_DECLARE_PRIVATE(qSlicerSMTKFileReaderPlugin);
  Q_DISABLE_COPY(qSlicerSMTKFileReaderPlugin);
};

#endif
