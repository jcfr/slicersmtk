//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.
 
  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/

#ifndef __vtkSlicerSMTKFileWriterLogic_h
#define __vtkSlicerSMTKFileWriterLogic_h

// Slicer includes
#include "vtkSlicerModuleLogic.h"

// SMTKFileWriter includes
#include "vtkSlicerSMTKFileWriterLogicExport.h"

class vtkMRMLScalarVolumeNode;
class vtkMRMLScalarVolumeDisplayNode;
class vtkMRMLScene;
class vtkMRMLVolumeHeaderlessStorageNode;
class vtkStringArray;

/// \ingroup SlicerRt_QtModules_SMTKFileWriter
class VTK_SLICER_SMTKFILEWRITER_LOGIC_EXPORT vtkSlicerSMTKFileWriterLogic :
  public vtkSlicerModuleLogic
{
public:
  static vtkSlicerSMTKFileWriterLogic *New();
  vtkTypeMacro(vtkSlicerSMTKFileWriterLogic, vtkSlicerModuleLogic);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  
  /// Export the current MRML \a scene as an smtk resource, i.e. the included
  /// model, volume, and segmentation MRML nodes.  Write the resource as a
  /// .aeva.smtk file which is a json format file, using the Write operation
  /// from aeva-session.
  /// \param scene A scene object, that usually is the current scene of the Slicer application.
  /// \param smtkFileName A string containing the Name of the output .aeva.smtk file.
  /// \param smtkExportDir A string containing the full path to the directory where the output file will be saved.
  bool ExportMRMLSceneToAevaSMTKResources(
    vtkMRMLScene* scene,
    const std::string& smtkFileName,
    const std::string& smtkExportDir
  );

protected:
  vtkSlicerSMTKFileWriterLogic();
  ~vtkSlicerSMTKFileWriterLogic() override;

private:
  vtkSlicerSMTKFileWriterLogic(const vtkSlicerSMTKFileWriterLogic&) = delete;
  void operator=(const vtkSlicerSMTKFileWriterLogic&) = delete;
};

#endif