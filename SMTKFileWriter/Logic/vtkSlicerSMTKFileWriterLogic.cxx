//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.
 
  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/
// STD includes
#include <string>
#include <vector>

// Qt includes
#include <QDebug>
#include <QDir>

// SMTKFileWriter includes
#include "vtkSlicerSMTKFileWriterLogic.h"

// VTK includes
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkDataSetAttributes.h>
#include <vtkGeometryFilter.h>
#include <vtkImageData.h>
#include <vtkNew.h>
#include <vtkPointSet.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>

// MRML includes
#include <vtkMRMLModelNode.h>
#include <vtkMRMLScalarVolumeDisplayNode.h>
#include <vtkMRMLScalarVolumeNode.h>
#include <vtkMRMLScene.h>
#include <vtkMRMLSegmentationNode.h>
#include <vtkMRMLSelectionNode.h>
#include <vtkMRMLVolumeNode.h>

// Slicer includes
#include <qSlicerCoreIOManager.h>
#include <vtkOrientedImageData.h>
#include <vtkSlicerApplicationLogic.h>

// smtk includes
#include <smtk/attribute/Attribute.h>
#include <smtk/attribute/ComponentItem.h>
#include <smtk/attribute/IntItem.h>
#include <smtk/attribute/VoidItem.h>
#include <smtk/model/Volume.h>
#include <smtk/operation/Manager.h>
#include <smtk/operation/MarkGeometry.h>
#include <smtk/resource/Manager.h>

// aeva-session includes
#include <smtk/session/aeva/Import.h>
#include <smtk/session/aeva/Predicates.h>
#include <smtk/session/aeva/Registrar.h>
#include <smtk/session/aeva/Resource.h>
#include <smtk/session/aeva/Write.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSlicerSMTKFileWriterLogic);

//----------------------------------------------------------------------------
vtkSlicerSMTKFileWriterLogic::vtkSlicerSMTKFileWriterLogic() = default;

//----------------------------------------------------------------------------
vtkSlicerSMTKFileWriterLogic::~vtkSlicerSMTKFileWriterLogic() = default;

//----------------------------------------------------------------------------
void vtkSlicerSMTKFileWriterLogic::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

namespace
{
typedef std::shared_ptr<smtk::attribute::Attribute> Result;

//----------------------------------------------------------------------------
// Generalized function to set filename property of a constructed smtk cell.
void setSMTKCellFilenameProperty(smtk::model::EntityRef& cell,
  const std::string& exportPath,
  const std::string& modelName,
  const std::string& extension
)
{
  if (cell.component() != nullptr)
  {
    std::string sanitizedModelName =
      qSlicerCoreIOManager::forceFileNameValidCharacters(QString(modelName.c_str())).toStdString();

    // Use santizedModelName as prefix to make the filenames human readable.
    // Use _<entity> as suffix (internally set as `uuid`) to guarantee
    // that filenames won't clash / overwrite each other.
    std::string modelFilePath = exportPath + "/"
      + (sanitizedModelName.empty() ? "" : sanitizedModelName + "_")
      + cell.entity().toString()
      + extension;

    // Update or insert filename property.
    if (cell.component()->properties().contains<std::string>("filename"))
    {
      cell.component()->properties().get<std::string>()["filename"] = modelFilePath;
    }
    else
    {
      cell.component()->properties().insert<std::string>("filename", modelFilePath);
    }
  }
}

//----------------------------------------------------------------------------
// Export a MRML model node as a component of the provided smtk resource.
// The model node is expected to be either a vtkUnstructuredGrid or vtkPolyData.
bool exportModelNodeToAevaSMTKResource(vtkMRMLModelNode* modelNode,
  const smtk::session::aeva::Resource::Ptr& resource,
  const std::string& exportPath
)
{
  // Create result object to satisfy API of CreateCellForData().
  using smtk::session::aeva::Import;
  auto import = Import::create();
  Result result = import->createResult(Import::Outcome::FAILED);

  long pointIdOffset = 0;
  long maxPointId = 0;
  long cellIdOffset = 0;
  long maxCellId = 0;
  const std::string pointKey("global point id offset");
  const std::string cellKey("global cell id offset");

  if (resource->properties().contains<long>(pointKey))
  {
    pointIdOffset = resource->properties().at<long>(pointKey);
  }
  if (resource->properties().contains<long>(cellKey))
  {
    cellIdOffset = resource->properties().at<long>(cellKey);
  }

  vtkDataSet* data = static_cast<vtkDataSet*>(modelNode->GetMesh());

  auto createdItems = result->findComponent("created");
  smtk::operation::MarkGeometry marker(resource);
  if (data)
  {
    // Remove any point/cell normals.
    auto* cdata = data->GetAttributes(vtkDataObject::CELL);
    auto* pdata = data->GetAttributes(vtkDataObject::POINT);
    if (cdata)
    {
      cdata->SetAttribute(nullptr, vtkDataSetAttributes::NORMALS);
    }
    if (pdata)
    {
      pdata->SetAttribute(nullptr, vtkDataSetAttributes::NORMALS);
    }

    auto modelName = std::string(modelNode->GetName());
    auto model = resource->addModel(3, 3, modelName);
    auto modelComp = model.component();
    modelComp->properties().get<std::string>()["aeva_datatype"] = "poly";
    createdItems->appendValue(modelComp);

    int count = 0;
    auto cell = smtk::session::aeva::CreateCellForData(model,
      data,
      createdItems,
      count,
      marker,
      pointIdOffset,
      cellIdOffset,
      maxPointId,
      maxCellId);

    // Determine extension of the file.
    std::string extension = data->IsA("vtkUnstructuredGrid") ? ".vtu" : ".vtp";

    // Add filename property to save files in a subdir.
    setSMTKCellFilenameProperty(cell, exportPath, modelName, extension);

    // Volume cells should have surface faces:
    if (cell.isVolume())
    {
      vtkNew<vtkGeometryFilter> boundaryFilter;
      // share points (and ids) with volume.
      boundaryFilter->MergingOff();
      boundaryFilter->SetInputDataObject(0, data);
      boundaryFilter->Update();
      // faces have global ids passed through from volume cells. Move to pedigree ids,
      // then new global ids are generated and they are offset from the volume cells.
      auto* geom = vtkDataSet::SafeDownCast(boundaryFilter->GetOutputDataObject(0));
      auto* pedigreeIds = geom->GetCellData()->GetGlobalIds();
      // take ownership (incr reference count) so it's not deleted when we clear global ids.
      pedigreeIds->Register(nullptr);
      geom->GetCellData()->SetGlobalIds(nullptr);
      geom->GetCellData()->SetPedigreeIds(pedigreeIds);
      pedigreeIds->FastDelete();
      cellIdOffset = maxCellId + 1;

      auto bdy = smtk::session::aeva::CreateCellForData(cell,
        geom,
        createdItems,
        count,
        marker,
        pointIdOffset,
        cellIdOffset,
        maxPointId,
        maxCellId);

      // Set path/filename for boundary surface.
      setSMTKCellFilenameProperty(bdy, exportPath, modelName, ".vtp");
    }
    // update the ID offsets per-file
    pointIdOffset = maxPointId + 1;
    cellIdOffset = maxCellId + 1;
  }

  // update the resource ID offsets.
  resource->properties().get<long>()[pointKey] = pointIdOffset;
  resource->properties().get<long>()[cellKey] = cellIdOffset;
  return true;
}

//----------------------------------------------------------------------------
// Add the provided vtkImageData object as a component to the provided smtk resource.
// Also sets the "filename" property as the export path and filename to be used by Write operation.
bool addVTKImageDataToAevaSMTKModelResource(
  vtkSmartPointer<vtkImageData> imageData,
  const smtk::session::aeva::Resource::Ptr& resource,
  const std::string& modelName,
  const std::string& exportPath
)
{
  auto model = resource->addModel(3, 3, modelName);
  auto modelComp = model.component();
  modelComp->properties().get<std::string>()["aeva_datatype"] = "image";
  auto volume = resource->addVolume();
  model.addCell(volume);

  auto const& session = resource->session();
  session->addStorage(volume.entity(), imageData);
  if (imageData->GetPointData()->GetScalars())
  {
    volume.setName(imageData->GetPointData()->GetScalars()->GetName());
  }

  // Set path/filename for the volume component.
  setSMTKCellFilenameProperty(volume, exportPath, modelName, ".vti");

  smtk::operation::MarkGeometry(resource).markModified(volume.component());
  return true;
}

//----------------------------------------------------------------------------
// Export a MRML volume node as a component of the provided smtk resource.
// The volume node is expected to be vtkImageData.
bool exportVolumeNodeToAevaSMTKResource(vtkMRMLVolumeNode* volumeNode,
  const smtk::session::aeva::Resource::Ptr& resource,
  const std::string& exportPath
)
{
  if (!volumeNode || !volumeNode->GetImageData())
  {
    qWarning() << "Unable to read image data from MRML Node: " << volumeNode->GetName() << ".";
    return false;
  }
  auto nodeImage = volumeNode->GetImageData();
  // Use DeepCopy to ensure that we don't affect on-going slicer operations,
  // since we will need to add spacing and origin info to the image object.
  vtkNew<vtkImageData> image;
  image->DeepCopy(nodeImage);
  image->SetSpacing(volumeNode->GetSpacing());
  image->SetOrigin(volumeNode->GetOrigin());

  auto modelName = std::string(volumeNode->GetName());
  return addVTKImageDataToAevaSMTKModelResource(image, resource, modelName, exportPath);
}

//----------------------------------------------------------------------------
// Export a MRML segmentation node as a component of the provided smtk resource.
// Each label is exported as a separate vtkImageData component.
// Therefore, a multi-label segmentation will result in multiple components.
// In that case, the filename property of each component is named using both:
// the segmentation node name and the label name (segment name).
bool exportSegmentationNodeToAevaSMTKResource(vtkMRMLSegmentationNode* segmentationNode,
  const smtk::session::aeva::Resource::Ptr& resource,
  const std::string& exportPath
)
{
  if (!segmentationNode || !segmentationNode->GetSegmentation())
  {
    return false;
  }

  bool successValue = true;
  std::vector<std::string> allSegmentIDs;
  segmentationNode->GetSegmentation()->GetSegmentIDs(allSegmentIDs);
  std::string groupName(segmentationNode->GetName());
  for (const auto& id : allSegmentIDs)
  {
    segmentationNode->CreateBinaryLabelmapRepresentation();
    vtkNew<vtkOrientedImageData> binaryLabelMap;
    segmentationNode->GetBinaryLabelmapRepresentation(id, binaryLabelMap);
    std::string segmentName = id;
    auto segment = segmentationNode->GetSegmentation()->GetSegment(id);
    if (segment)
    {
      segmentName = std::string(segment->GetName());
    }
    successValue &= addVTKImageDataToAevaSMTKModelResource(binaryLabelMap, resource, groupName + "_" + segmentName, exportPath);
  }
  return successValue;
}

//----------------------------------------------------------------------------
// Create a temporary scene to use for transform hardening without affecting the existing slicer scene.
vtkSmartPointer<vtkMRMLScene> createTemporaryScene(vtkMRMLScene* scene)
{
  vtkNew<vtkMRMLScene> temporaryScene;
  scene->CopyDefaultNodesToScene(temporaryScene);
  scene->CopyRegisteredNodesToScene(temporaryScene);
  scene->CopySingletonNodesToScene(temporaryScene);
  temporaryScene->SetDataIOManager(scene->GetDataIOManager());
  return temporaryScene;
}

} // anonymous namespace

//----------------------------------------------------------------------------
bool vtkSlicerSMTKFileWriterLogic::ExportMRMLSceneToAevaSMTKResources(
  vtkMRMLScene* scene,
  const std::string& smtkFileName,
  const std::string& smtkExportDir
)
{
  // Create an smtk resource manager to hold all the MRML nodes.
  smtk::resource::Manager::Ptr resManager = smtk::resource::Manager::create();
  smtk::session::aeva::Registrar::registerTo(resManager);
  smtk::session::aeva::Resource::Ptr resource = nullptr;
  smtk::session::aeva::Session::Ptr session = nullptr;
  resource = resManager->create<smtk::session::aeva::Resource>();
  session = smtk::session::aeva::Session::create();
  resource->setSession(session);

  std::string smtkFilePath = smtkExportDir + "/" + smtkFileName;
  resource->setLocation(smtkFilePath);

  // Determine subdir for model files.
  auto baseName = smtkFileName.substr(0, smtkFileName.find_first_of('.'));
  std::string exportSubDir = smtkExportDir + "/" + baseName;

  // Construct the subdir.
  QDir dir(exportSubDir.c_str());
  if (!dir.mkpath("."))
  {
    qWarning() << "Cannot create output subdir: " << exportSubDir.c_str();
    return false;
  }

  // Create temporary scene object for transform hardening
  auto temporaryScene = createTemporaryScene(scene);
  // Now create smtk resources for each MRML node and add it to the resManager.
  const int numNodes = scene->GetNumberOfNodes();
  for (int i = 0; i < numNodes; ++i)
  {
    auto node = scene->GetNthNode(i);

    if (
      // Skip if hidden node,
      node->GetHideFromEditors() ||
      // OR, skip if not a model, volume, or segmentation
      !node->IsA("vtkMRMLModelNode")
      && !node->IsA("vtkMRMLVolumeNode")
      && !node->IsA("vtkMRMLSegmentationNode")
      )
    {
      continue;
    }

    auto temporaryStorableNode = qSlicerCoreIOManager::applyTransformHardening(vtkMRMLStorableNode::SafeDownCast(node), temporaryScene);
    if (!temporaryStorableNode)
    {
      continue;
    }
    // Copy semantic name of node to the temporary node, since it is used in the filename as prefix.
    temporaryStorableNode->SetName(node->GetName());

    // process for vtkUnstructuredGrid and vtkPolyData:
    if (temporaryStorableNode->IsA("vtkMRMLModelNode"))
    {
      auto modelNode = vtkMRMLModelNode::SafeDownCast(temporaryStorableNode);
      // for all non-trivial models
      if (modelNode->GetMesh()->GetNumberOfPoints() > 4)
      {
        exportModelNodeToAevaSMTKResource(modelNode, resource, exportSubDir);
      }
    }
    // process for volume/image nodes
    else if (temporaryStorableNode->IsA("vtkMRMLVolumeNode"))
    {
      auto volumeNode = vtkMRMLVolumeNode::SafeDownCast(temporaryStorableNode);
      exportVolumeNodeToAevaSMTKResource(volumeNode, resource, exportSubDir);
    }
    // process for segmentation nodes
    else if (temporaryStorableNode->IsA("vtkMRMLSegmentationNode"))
    {
      auto segmentationNode = vtkMRMLSegmentationNode::SafeDownCast(temporaryStorableNode);
      exportSegmentationNodeToAevaSMTKResource(segmentationNode, resource, exportSubDir);
    }
  }

  resManager->add(resource);

  // Directly construct and use the Write operator, so that we can
  // switch off the "copy all data with resource" flag.
  using smtk::session::aeva::Write;
  auto writeOp = smtk::session::aeva::Write::create();
  writeOp->parameters()->associate(resource);
  auto voidItem = writeOp->parameters()->findVoid("copy all data with resource");
  if (voidItem)
  {
    voidItem->setIsEnabled(false);
  }
  else
  {
    qWarning() << "Unable to switch-off \"copy all data with resource\" flag.";
  }

  Write::Result result = writeOp->operate();
  bool success = result->findInt("outcome")->value() == static_cast<int>(Write::Outcome::SUCCEEDED);

  // Forward smtk log records to Slicer's error log.
  auto logger = writeOp->log();
  qInfo() << logger.convertToString().c_str();

  if (success)
  {
    qInfo() << "MRML scene successfully exported as aeva SMTK file: " << smtkFilePath.c_str();
    qInfo() << "Model, Segmentation, and Volume nodes successfully exported as Aeva SMTK resources into directory: " << exportSubDir.c_str();
  }
  else
  {
    qWarning() << "Failed to export MRML scene as aeva SMTK file: " << smtkFilePath.c_str();
  }
  return success;
}
