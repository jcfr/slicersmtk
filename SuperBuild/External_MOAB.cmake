
set(proj MOAB)

# Set dependency list
set(${proj}_DEPENDS
  Eigen3
  )

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})
  message(FATAL_ERROR "Enabling ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED MOAB_DIR AND NOT EXISTS ${MOAB_DIR})
  message(FATAL_ERROR "MOAB_DIR variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})

  ExternalProject_SetIfNotDefined(
    ${CMAKE_PROJECT_NAME}_${proj}_GIT_REPOSITORY
    "https://bitbucket.org/fathomteam/moab.git"
    QUIET
    )

  ExternalProject_SetIfNotDefined(
    ${CMAKE_PROJECT_NAME}_${proj}_GIT_TAG
    "5.3.1"
    QUIET
    )

  set(EP_SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj})
  set(EP_BINARY_DIR ${CMAKE_BINARY_DIR}/${proj}-build)
  set(EP_INSTALL_DIR ${CMAKE_BINARY_DIR}/${proj}-install)

  # Workaround improper generation of target file when destination associated
  # with "install(EXPORT MOABTargets DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/MOAB/)"
  # start with "./"
  if(NOT APPLE)
    set(Slicer_INSTALL_THIRDPARTY_LIB_DIR ${Slicer_THIRDPARTY_LIB_DIR})
  endif()

  ExternalProject_Add(${proj}
    ${${proj}_EP_ARGS}
    GIT_REPOSITORY "${${CMAKE_PROJECT_NAME}_${proj}_GIT_REPOSITORY}"
    GIT_TAG "${${CMAKE_PROJECT_NAME}_${proj}_GIT_TAG}"
    SOURCE_DIR ${EP_SOURCE_DIR}
    BINARY_DIR ${EP_BINARY_DIR}
    INSTALL_DIR ${EP_INSTALL_DIR}
    CMAKE_CACHE_ARGS
      # Compiler settings
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${ep_common_c_flags}
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${ep_common_cxx_flags}
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_STANDARD_REQUIRED:BOOL=${CMAKE_CXX_STANDARD_REQUIRED}
      -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
      # Output directories
      -DCMAKE_RUNTIME_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_BIN_DIR}
      -DCMAKE_LIBRARY_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_LIB_DIR}
      -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY:PATH=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
      # Install directories
      -DCMAKE_INSTALL_PREFIX:PATH=${EP_INSTALL_DIR}
      -DMOAB_INSTALL_RUNTIME_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DMOAB_INSTALL_LIBRARY_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DCMAKE_INSTALL_LIBDIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR} # Skip default initialization by GNUInstallDirs CMake module
      # Options copied from https://gitlab.kitware.com/cmb/cmb-superbuild/-/blob/ee2a5b51a842194b5979aa74bc6cb3b26b5721c6/projects/moab.cmake
      -DCMAKE_VISIBILITY_INLINES_HIDDEN:BOOL=ON
      -DENABLE_BLASLAPACK:BOOL=OFF
      -DMOAB_HAVE_EIGEN:BOOL=ON
      -DMOAB_USE_SZIP:BOOL=ON
      -DMOAB_USE_CGM:BOOL=OFF
      -DMOAB_USE_CGNS:BOOL=OFF
      -DMOAB_USE_MPI:BOOL=OFF
      -DMOAB_BUILD_HEXMODOPS:BOOL=OFF
      -DMOAB_BUILD_MBCONVERT:BOOL=OFF
      -DMOAB_BUILD_MBCOUPLER:BOOL=OFF
      -DMOAB_BUILD_MBDEPTH:BOOL=OFF
      -DMOAB_BUILD_MBGSETS:BOOL=OFF
      -DMOAB_BUILD_MBHONODES:BOOL=OFF
      -DMOAB_BUILD_MBMEM:BOOL=OFF
      -DMOAB_BUILD_MBPART:BOOL=OFF
      -DMOAB_BUILD_MBQUALITY:BOOL=OFF
      -DMOAB_BUILD_MBSIZE:BOOL=OFF
      -DMOAB_BUILD_MBSKIN:BOOL=OFF
      -DMOAB_BUILD_MBSLAVEPART:BOOL=OFF
      -DMOAB_BUILD_MBSURFPLOT:BOOL=OFF
      -DMOAB_BUILD_MBTAGPROP:BOOL=OFF
      -DMOAB_BUILD_MBTEMPEST:BOOL=OFF
      -DMOAB_BUILD_MBUMR:BOOL=OFF
      -DMOAB_BUILD_SPHEREDECOMP:BOOL=OFF
      -DENABLE_TESTING:BOOL=OFF
      # Options explicitly disabled in SlicerSMTK
      -DENABLE_FBIGEOM:BOOL=OFF
      -DENABLE_FORTRAN:BOOL=OFF
      -DENABLE_IMESH:BOOL=OFF
      -DENABLE_IREL:BOOL=OFF
      # MOAB provides its own find module (See MOAB/config/FindEigen3.cmake)
      # requiring EIGEN3_DIR to be set to the include directories.
      -DEIGEN3_DIR:PATH=${Eigen3_INCLUDE_DIR}
      -DMOAB_USE_HDF:BOOL=OFF
      -DENABLE_HDF5:BOOL=OFF
      -DMOAB_USE_NETCDF:BOOL=OFF
      -DENABLE_NETCDF:BOOL=OFF
    DEPENDS
      ${${proj}_DEPENDS}
    )
  set(${proj}_DIR ${EP_INSTALL_DIR}/${Slicer_INSTALL_THIRDPARTY_LIB_DIR}/cmake/MOAB)

  #-----------------------------------------------------------------------------
  # Launcher setting specific to build tree

  set(${proj}_LIBRARY_PATHS_LAUNCHER_BUILD ${EP_INSTALL_DIR}/${Slicer_INSTALL_THIRDPARTY_LIB_DIR})
  mark_as_superbuild(
    VARS ${proj}_LIBRARY_PATHS_LAUNCHER_BUILD
    LABELS "LIBRARY_PATHS_LAUNCHER_BUILD"
    )

  #-----------------------------------------------------------------------------
  # Launcher setting specific to install tree

  # NA

else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(${proj}_DIR:PATH)

ExternalProject_Message(${proj} "${proj}_DIR:${${proj}_DIR}")

