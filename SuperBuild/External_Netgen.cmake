
set(proj Netgen)

# Set dependency list
set(${proj}_DEPENDS
  ""
  )
if(DEFINED Slicer_SOURCE_DIR)
  # Extension is bundled in a custom application
  list(APPEND ${proj}_DEPENDS
    zlib
    )
endif()

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})
  message(FATAL_ERROR "Enabling ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED Netgen_DIR AND NOT EXISTS ${Netgen_DIR})
  message(FATAL_ERROR "Netgen_DIR variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})

  ExternalProject_SetIfNotDefined(
    ${CMAKE_PROJECT_NAME}_${proj}_GIT_REPOSITORY
    "https://github.com/KitwareMedical/netgen.git"
    QUIET
    )

  ExternalProject_SetIfNotDefined(
    ${CMAKE_PROJECT_NAME}_${proj}_GIT_TAG
    "d06f2f4f9c62564f45d1d94955fb0afc8ce03fc9" # slicersmtk-v6.2.1810-2018.11.23-7934a3487
    QUIET
    )

  set(EP_SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj})
  set(EP_BINARY_DIR ${CMAKE_BINARY_DIR}/${proj}-build)
  set(EP_INSTALL_DIR ${CMAKE_BINARY_DIR}/${proj}-install)

  # Ensure setting GIT_SUBMODULES to "" does not initialize any submodules
  # See https://cmake.org/cmake/help/latest/policy/CMP0097.html
  if(POLICY CMP0097)
    cmake_policy(SET CMP0097 NEW)
  endif()

  # Workaround improper generation of target file when destination associated
  # with "install(EXPORT MOABTargets DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/MOAB/)"
  # start with "./"
  if(NOT APPLE)
    set(Slicer_INSTALL_THIRDPARTY_LIB_DIR ${Slicer_THIRDPARTY_LIB_DIR})
  endif()

  ExternalProject_Add(${proj}
    ${${proj}_EP_ARGS}
    GIT_REPOSITORY "${${CMAKE_PROJECT_NAME}_${proj}_GIT_REPOSITORY}"
    GIT_TAG "${${CMAKE_PROJECT_NAME}_${proj}_GIT_TAG}"
    GIT_SUBMODULES ""
    SOURCE_DIR ${EP_SOURCE_DIR}
    BINARY_DIR ${EP_BINARY_DIR}
    INSTALL_DIR ${EP_INSTALL_DIR}
    CMAKE_CACHE_ARGS
      # Compiler settings
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${ep_common_c_flags}
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${ep_common_cxx_flags}
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_STANDARD_REQUIRED:BOOL=${CMAKE_CXX_STANDARD_REQUIRED}
      -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
      # Output directories
      -DCMAKE_RUNTIME_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_BIN_DIR}
      -DCMAKE_LIBRARY_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_LIB_DIR}
      -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY:PATH=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
      # Install directories
      -DCMAKE_INSTALL_PREFIX:PATH=${EP_INSTALL_DIR}
      -DNetgen_INSTALL_RUNTIME_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DNetgen_INSTALL_LIBRARY_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DNG_INSTALL_DIR_BIN:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DNG_INSTALL_DIR_LIB:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DNG_INSTALL_DIR_CMAKE:STRING=lib/cmake/netgen
      -DNG_INSTALL_DIR_RES:STRING=share
      -DNG_INSTALL_DIR_INCLUDE:STRING=include
      # Options copied from https://gitlab.kitware.com/cmb/cmb-superbuild/-/blob/ee2a5b51a842194b5979aa74bc6cb3b26b5721c6/projects/netgen.cmake
      -DUSE_SUPERBUILD:BOOL=OFF
      -DUSE_OCC:BOOL=OFF
      -DUSE_PYTHON:BOOL=OFF
      -DUSE_INTERNAL_TCL:BOOL=OFF
      -DUSE_GUI:BOOL=OFF
      -DUSE_MPI4PY:BOOL=OFF
      -DUSE_NATIVE_ARCH:BOOL=OFF
      # Dependencies
      -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}
      -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY}
    DEPENDS
      ${${proj}_DEPENDS}
    )
  set(${proj}_DIR ${EP_INSTALL_DIR}/lib/cmake/netgen)

  #-----------------------------------------------------------------------------
  # Launcher setting specific to build tree

  set(${proj}_LIBRARY_PATHS_LAUNCHER_BUILD ${EP_INSTALL_DIR}/${Slicer_INSTALL_THIRDPARTY_LIB_DIR})
  mark_as_superbuild(
    VARS ${proj}_LIBRARY_PATHS_LAUNCHER_BUILD
    LABELS "LIBRARY_PATHS_LAUNCHER_BUILD"
    )

  #-----------------------------------------------------------------------------
  # Launcher setting specific to install tree

  # NA

else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(${proj}_DIR:PATH)

ExternalProject_Message(${proj} "${proj}_DIR:${${proj}_DIR}")

