set(proj smtk)

# Set dependency list
set(${proj}_DEPENDS
  ${smtk_EXTERNAL_PROJECT_DEPENDENCIES}
  )
set(_vtk_modules_depends
  vtkIOParallelExodus
  )
if(NOT DEFINED Slicer_SOURCE_DIR)
  # Extension is build standalone against Slicer itself built
  # against VTK without the relevant modules enabled.
  list(APPEND ${proj}_DEPENDS
    ${_vtk_modules_depends}
    )
endif()

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
  message(FATAL_ERROR "Enabling ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED smtk_DIR AND NOT EXISTS ${smtk_DIR})
  message(FATAL_ERROR "smtk_DIR variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})

  ExternalProject_SetIfNotDefined(
    ${SUPERBUILD_TOPLEVEL_PROJECT}_${proj}_GIT_REPOSITORY
    "https://gitlab.kitware.com/cmb/smtk.git"
    QUIET
    )

  ExternalProject_SetIfNotDefined(
    ${SUPERBUILD_TOPLEVEL_PROJECT}_${proj}_GIT_TAG
    "86cd6d18c5fb9b3a28ca0d30c7d8aa778f2a86fd"
    QUIET
    )

  set(EP_SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj})
  set(EP_BINARY_DIR ${CMAKE_BINARY_DIR}/${proj}-build)

  set(EXTERNAL_PROJECT_OPTIONAL_CMAKE_CACHE_ARGS)
  foreach(_name IN LISTS _vtk_modules_depends)
    list(APPEND EXTERNAL_PROJECT_OPTIONAL_CMAKE_CACHE_ARGS
      -D${_name}_DIR:PATH=${${_name}_DIR}
      )
    set(_enabled "OFF")
    if(TARGET ${_name})
      set(_enabled "ON")
    endif()
    ExternalProject_Message(${proj} "${proj}[${_name}:${_enabled}]")
  endforeach()

  ExternalProject_Add(${proj}
    ${${proj}_EP_ARGS}
    GIT_REPOSITORY "${${SUPERBUILD_TOPLEVEL_PROJECT}_${proj}_GIT_REPOSITORY}"
    GIT_TAG "${${SUPERBUILD_TOPLEVEL_PROJECT}_${proj}_GIT_TAG}"
    SOURCE_DIR ${EP_SOURCE_DIR}
    BINARY_DIR ${EP_BINARY_DIR}
    CMAKE_CACHE_ARGS
      # Compiler settings
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${ep_common_c_flags}
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${ep_common_cxx_flags}
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_STANDARD_REQUIRED:BOOL=${CMAKE_CXX_STANDARD_REQUIRED}
      -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
      # Output directories
      -DCMAKE_RUNTIME_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_BIN_DIR}
      -DCMAKE_LIBRARY_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_LIB_DIR}
      -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY:PATH=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
      -DCMAKE_INSTALL_LIBDIR:STRING=lib # Specify value to avoid initialization to lib64 by GNUInstallDirs CMake module
      # Install directories
      -Dsmtk_RUNTIME_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -Dsmtk_LIBRARY_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      # Options
      -DSMTK_ENABLE_TESTING:BOOL=OFF
      # Dependencies
      -DBoost_DIR:PATH=${Boost_DIR}
      -DLibArchive_INCLUDE_DIR:PATH=${LibArchive_INCLUDE_DIR}
      -DLibArchive_LIBRARY:FILEPATH=${LibArchive_LIBRARY}
      -DMOAB_DIR:PATH=${MOAB_DIR}
      -Dnlohmann_json_DIR:PATH=${nlohmann_json_DIR}
      -Dpegtl_DIR:PATH=${pegtl_DIR}
      -DTBB_DIR:PATH=${TBB_DIR}
      -DVTK_DIR:PATH=${VTK_DIR}
      ${EXTERNAL_PROJECT_OPTIONAL_CMAKE_CACHE_ARGS}
    INSTALL_COMMAND ""
    DEPENDS
      ${${proj}_DEPENDS}
    )

  set(${proj}_DIR ${EP_BINARY_DIR})

  #-----------------------------------------------------------------------------
  # Launcher setting specific to build tree

  set(_lib_subdir ${Slicer_THIRDPARTY_LIB_DIR})
  if(WIN32)
    set(_lib_subdir ${Slicer_THIRDPARTY_BIN_DIR})
  endif()

  set(${proj}_LIBRARY_PATHS_LAUNCHER_BUILD ${EP_BINARY_DIR}/${_lib_subdir})
  mark_as_superbuild(
    VARS ${proj}_LIBRARY_PATHS_LAUNCHER_BUILD
    LABELS "LIBRARY_PATHS_LAUNCHER_BUILD"
    )

  #-----------------------------------------------------------------------------
  # Launcher setting specific to install tree

  # NA

else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(${proj}_DIR:PATH)
